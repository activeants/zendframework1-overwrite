# [![Active Ants](http://activeants.nl/content/themes/active-ants/img/logo.png)](http://activeants.nl/) 

## zendframework1 overwrite 
A library to overwrite classes from old versions of the Zend1 framework used for example by Magento 2.

## Motivation
We found out that it was not possible to use email addresses which uses TLD's active since Fri Nov 28 07:07:01 2014 UTC.(see Magento2 issue https://github.com/magento/magento2/issues/4547)

So we decided to create a (for now) small library with 1 class to overcome this issue.
 
## Installation
```
composer require activeants/zendframework1-overwrite
```

## Issues
Feel free to report any issues for this project using the integrated [Bitbucket issue tracker](https://bitbucket.org/activeants/zendframework1-overwrite/issues), we will try to get back to issues as fast as possible.

### License
The zendframework1 overwrite library is free software, and is released under the terms of the OSL 3.0 license. See LICENSE.md for more information.

## Credits
[Active Ants](http://activeants.nl/) provides e-commerce efulfilment services for webshops. We store products, pick, pack and ship them. But we do much more. With unique efulfilment solutions we make our clients, the webshops, more successful.

~ The [Active Ants](http://activeants.nl/) software development team.